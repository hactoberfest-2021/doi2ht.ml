<?php

function get_DataCite($doi) {
	$doi = urlencode($doi);
	//	Call DataCite
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, "https://data.crosscite.org/application/vnd.schemaorg.ld+json/{$doi}");
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$output = curl_exec($curl);

	//	Has it worked?
	if (!curl_errno($curl)) {
	switch ($http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE)) {
		case 200:  # OK
			curl_close($curl);
			return json_decode($output, true);
		default:
			curl_close($curl);
			return false;
		}
	}
}

function get_CrossRef($doi) {
	$doi = urlencode($doi);

	//	Set up the libraries
	require __DIR__ . '/vendor/autoload.php';

	//	Set up the API connection to CrossRef
	$client = new RenanBr\CrossRefClient();
	$client->setCache(new voku\cache\CachePsr16());
	$client->setUserAgent('doi2html5 (https://shkspr.mobi/blog/; mailto:crossref@shkspr.mobi)');

	//	API call
	try {
		$api_response = $client->request("works/{$doi}");
	} catch (GuzzleHttp\Exception\RequestException $e) {
		//	Generic error message. Might be worth displaying different messages based on error type.
		// var_dump($e);
		return false;
	}
	//	Get the important part of the response
	$data = $api_response["message"];
	//	Convert to Schema before returning?
	foreach ($data["author"] as &$a) {
		if (array_key_exists("ORCID", $a)) {
			$a["@id"] = $a["ORCID"];
		}
		if (array_key_exists("family", $a)) {
			$a["familyName"] = $a["family"];
		}
		if (array_key_exists("given", $a)) {
			$a["givenName"] = $a["given"];
		}
	}
	if (array_key_exists("title", $data) ) {
		$data["name"] = $data["title"][0];
	}
	return $data;
}

// Set Up Flags
$success = false;
$service = "";

echo "<!doctype html><html lang=\"en-gb\"><head><title>DOI to Semantic HTML5</title>";
echo '<link rel=stylesheet href="https://picnicss.com/style.min.css">';
echo '<script src="clipboard.min.js"></script>';
echo '<base target="_blank">';
echo '</head><body><main>';
echo '<header style="text-align:center;margin-bottom:-1em;margin-top:1em;"><h1>DOI 2 HTML</h1></header>';
echo '<div id="content" class="visual flex one two-800">';

//	Has a ?doi= been sent?
if( isset( $_GET['doi'] ) ) {
	$input_doi = $_GET['doi'];

	//	Try DataCite first
	$data = get_DataCite($input_doi);

	//	If it can't be found, try CrossRef
	if (false == $data) {
		$data = get_CrossRef($input_doi);
		$service = "CrossRef";
	} else {
		$service = "DataCite";
	}

	if (false == $data) {
		$success = false;
		echo '<div class="content">';
		echo '<section>';
		echo '<h2 style="color:#ee0d00;">Error</h2>';
		echo "<p>Unable to find <samp>" . htmlspecialchars($input_doi) . "</samp> ";
		echo "on either DataCite or CrossRef.";
		echo "<p>Please check and try again.";
		echo '</section>';
		echo "</div>";
	} else {
		$success = true;
	}

	if ($success) {

		//	Start the citation
		$citation_html = "<span itemscope itemtype=\"http://schema.org/ScholarlyArticle\"><span itemprop=\"citation\">";

		//	Get all the authors
		$authors = array();

		//	Is this a single author?
		//	Place the author in an array, as though there were multiple authors
		if (array_key_exists("@type", $data["author"])) {
			$data["author"] = array($data["author"]);
		}

		//	Build the author list HTML
		foreach ($data["author"] as $a) {
			//	Are all authors human?
			$author_html = "<span itemprop=\"author\" itemscope itemtype=\"http://schema.org/Person\">";

			//	Add the ORCID reference if it exists
			//	Should this be a hyperlink?
			// if (array_key_exists("ORCID", $a)) {
			// 	$author_html .= "<link itemprop=\"url\" href=\"" . htmlspecialchars($a["ORCID"]) . '"/>';
			// }
			if (array_key_exists("@id", $a)) {
				$orcid = htmlspecialchars($a["@id"]);
				$author_html .= "<link itemprop=\"url\" href=\"" . $orcid . '"/>';
			}

			$familyName = htmlspecialchars($a["familyName"]);
			$givenName  = htmlspecialchars($a["givenName"]);

			//	Display as "FamilyName, FirstName"
			$author_html .= "<span itemprop=\"name\">"
			                  ."<span itemprop=\"familyName\">{$familyName}</span>"
									."<span>, </span>"
								   ."<span itemprop=\"givenName\">{$givenName}</span>"
							   ."</span>"
							."</span>";

			$authors[] = $author_html;
		}

		//	Add an "&" between authors
		$i = 0;
		$author_count = count($authors);
		foreach ($authors as $author) {
			$citation_html .= $author;
			if (++$i != $author_count) {
				//	Don't add "&" on last author
				$citation_html .= "<span> & </span>";
			}
		}

		//	Title and any language information
		if (array_key_exists("name", $data) ) {
			$headline = htmlspecialchars($data["name"]);

			if (null != $headline) {
				if (array_key_exists("inLanguage", $data)) {
					$lang = htmlspecialchars($data["language"]);
					$citation_html .= " <q><cite itemprop=\"headline\" lang=\"{$lang}\">"
					. "<span itemprop=\"inLanguage\" content=\"{$lang}\">{$headline}</span></cite></q> ";
				}
				else {
					$citation_html .= " <q><cite itemprop=\"headline\">{$headline}</cite></q> ";
				}
			}
		}

		//	Convert the date to Unix epoch format
		//	Format the date as a single year
		//$date = DateTime::createFromFormat('U', strtotime("2020-01"));
		//$date->format('Y');
		if ( array_key_exists("datePublished", $data) ){
			$date = DateTime::createFromFormat('U',  strtotime($data["datePublished"]) );
			$year = $date->format('Y');
			$date_type = "datePublished";
		} else if ( array_key_exists("dateCreated", $data) ){
			$date = DateTime::createFromFormat('U',  strtotime($data["dateCreated"]) );
			$year = $date->format('Y');
			$date_type = "dateCreated";
		} else if ( array_key_exists("dateIssued", $data) ){
			$date = DateTime::createFromFormat('U',  strtotime($data["dateIssued"]) );
			$year = $date->format('Y');
			$date_type = "dateIssued";
		}

		$citation_html .= "<span>(</span><time itemprop=\"{$date_type}\" datetime=\"{$year}\">{$year}</time><span>)</span> ";
		//	Add the date
		// if (array_key_exists("published-print", $data) && $data["published"]["date-parts"][0][0] != ""){
		// 	$year = htmlspecialchars($data["published-print"]["date-parts"][0][0]);
		// 	$citation_html .= '<span>(</span><time itemprop="datePublished" datetime="'.$year.'">' . $year . '</time><span>)</span> ';
		// } elseif (array_key_exists("issued", $data) && $data["issued"]["date-parts"][0][0] != ""){
		// 	$year = htmlspecialchars($data["issued"]["date-parts"][0][0]);
		// 	$citation_html .= '<span>(</span><time itemprop="datePublished" datetime="'.$year.'">' . $year . '</time><span>)</span> ';
		// } elseif (array_key_exists("created", $data) && $data["created"]["date-parts"][0][0] != ""){
		// 	$datetime = htmlspecialchars($data["created"]["date-time"]);
		// 	$year     = htmlspecialchars($data["created"]["date-parts"][0][0]);
		// 	$citation_html .= '<span>(</span><time itemprop="datePublished" datetime="'.$datetime.'">' . $year . '</time><span>)</span> ';
		// } elseif (array_key_exists("deposited", $data) && $data["deposited"]["date-parts"][0][0] != ""){
		// 	$datetime = htmlspecialchars($data["deposited"]["date-time"]);
		// 	$year     = htmlspecialchars($data["deposited"]["date-parts"][0][0]);
		// 	$citation_html .= '<span>(</span><time itemprop="datePublished" datetime="'.$datetime.'">' . $year . '</time><span>)</span> ';
		// }

		//	Publisher
		if (array_key_exists("publisher", $data)) {
			$type = htmlspecialchars($data["publisher"]["@type"]);
			$name = htmlspecialchars($data["publisher"]["name"]);
			$citation_html .= "<span itemprop=\"publisher\" itemscope itemtype=\"http://schema.org/{$type}\">"
			."<span itemprop=\"name\">{$name}</span></span><span>.</span> ";
		}

		//	Publication
		if (array_key_exists("periodical", $data)) {
			$type = htmlspecialchars($data["periodical"]["@type"]);
			$name = htmlspecialchars($data["periodical"]["name"]);

			if (null != $name) {
				$citation_html .= "<span itemprop=\"publication\" itemscope itemtype=\"http://schema.org/{$type}\">"
				."<span itemprop=\"name\">{$name}</span></span><span>.</span> ";
			}
		}

		//	Page number information
		if (array_key_exists("pagination", $data)){
			$page = htmlspecialchars($data["pagination"]);
			$citation_html .= "<span> Page: </span><span itemprop=\"pagination\">{$page}</span><span>. </span>";
		} else if (array_key_exists("pageStart", $data)){
			$page = htmlspecialchars($data["pageStart"]);
			$citation_html .= "<span> Page: </span><span itemprop=\"pageStart\">{$page}</span><span>. </span>";
		}

		// //	Publication
		// if (array_key_exists("container-title", $data) && ($data["container-title"][0] != "") ){
		// 	$citation_html .= '<span itemprop="publication">'
		// 		. htmlspecialchars($data["container-title"][0]).'</span><span>.</span> ';
		// }

		//	DOI link

		if (array_key_exists("@id", $data)){
			$doi_url = htmlspecialchars($data["@id"]);
			$citation_html .= "DOI: <a itemprop=\"url\" href=\"{$doi_url}\">{$doi_url}</a>";
		}

		// if (array_key_exists("DOI", $data)){
		// 	$doi = htmlspecialchars($data["DOI"]);
		// 	$doi_url = "https://doi.org/" . $doi;
		// 	$citation_html .= '<a itemprop="url" href="'.$doi_url.'">doi:'.$doi.'</a>';
		// }

		//	End the citation
		$citation_html .= '</span></span>';

		//	Print the page HTML
		echo '<div class="content">';
		echo '<h2>Your Semantic Citation</h2>';

		// var_dump($data);
		echo "<p>" . $citation_html . "</p>";
		echo "<h3>Copy This Code:</h3><br>";

		echo "<textarea readonly id=\"output\" style=\"font-family:monospace;height:10em;margin-bottom:0.6em\">";
		echo htmlspecialchars($citation_html);
		echo "</textarea>";

		echo '<button class="btn" data-clipboard-action="copy" data-clipboard-target="#output">Copy to clipboard</button>';
		$structured_url  = "https://search.google.com/test/rich-results?url=https%3A%2F%2Fdoi2ht.ml%2F%3Fdoi%3D";
		$structured_url .= urlencode($input_doi);
		$structured_url .= "&user_agent=2";
		echo "<p><a href=\"{$structured_url}\">Test this structured data with Google</a>";


		echo "</div>";

	}
} else {
	?>
<div class="content">
	<h1>Convert DOI to HTML</h1>
	<p>This website helps you convert your boring old DOI to a <em>fully semantic</em> HTML5 citation.
	<p>Stick the citation on your website and benefit from:
		<ul>
			<li>Enhanced Search Engine Optimisation
			<li>Rich Snippets on search results
			<li>Better metadata for automated tools
		</ul>
	<p>Enter a valid DOI in the box to get started.
	</div>

	<?php
}

?>
<div class="card">
	<section>
		<form action="/" method="GET" target="_self">
			<div>
				<h2><label for="doi">Enter your DOI here:</label></h2><br>
				<input name="doi" id="doi" title="Valid DOI" value=""
				pattern="\b10\.(\d+\.*)+[\/](([^\s\.])+\.*)+\b"
				placeholder="10.1016/j.solener.2018.03.020">
			</div>
			<button>Convert DOI to Semantic HTML5 Citation</button>
		</form>
		<br>
		<p>We do not collect <em>any</em> of your personal data.
		<p>This website makes use of the <a href="https://support.datacite.org/docs/api">DataCite API</a> and the <a href="https://www.crossref.org/education/retrieve-metadata/rest-api/">CrossRef API</a>. None of your personal data is sent to either service.
		<p>This is a "best effort" service with no guarantee; please check the output carefully.
		<p>Created by <a href="https://shkspr.mobi/blog/">Terence Eden</a>.
		<p><a href="https://gitlab.com/edent/doi2ht.ml">Open Source Code available on GitLab</a>.
	</section>
</div>
<?php

if ($success) {
	echo "<div class=\"full visual\">";
	echo "<details><summary>▶️ Pretty-Print view</summary>";
	echo "<pre>";
	// Specify configuration
	$config = array(
				'indent'         => true,
				'input-xml'      => true,
				'output-html'    => true,
				'show-body-only' => true,
				'wrap'           => 200);

	// Tidy
	$tidy = new tidy;
	$tidy->parseString($citation_html, $config, 'utf8');

	echo htmlspecialchars($tidy);

	echo "</pre>";
	echo "</details>";
	echo "<details><summary>🐞 Debug data</summary>";
	echo "<pre>{$service}\n";
	print_r($data);
	echo "</pre>";
	echo "</details>";
	echo "</div>";
}
echo "<script>new ClipboardJS('.btn');</script>";
echo "</div></main></body></html>";
die();